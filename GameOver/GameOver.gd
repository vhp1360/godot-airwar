extends Control

var strGmOvrPth: String

func _ready():
	Utility.gGameOver = self

func _on_TextureButton_pressed():
	get_tree().change_scene(Utility.gWorld1_path)
	self.queue_free()

func _on_GameOver_tree_entered():
	Utility.fUpdateGameData()

func _exit_tree():
	Utility.fFinalWrite()

func _on_TextureButton2_pressed():
	get_tree().change_scene(Utility.gAbout_path)
