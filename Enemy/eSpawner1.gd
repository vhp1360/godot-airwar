extends Node2D

var spawnPositions

const Enemy1=preload("res://Enemy/EnemyTrump1.tscn")

func _ready():
	Utility.geSpawner1 = self
	spawnPositions=$Positions

func _on_Timer_timeout():
	if Utility.gPlayerScore>Utility.gEnemy01ChangeSped: $Timer.wait_time=1
	fSpawn_Enemy1()

func fSpawn_Enemy1():
	var enemy = Enemy1.instance()
	get_tree().current_scene.add_child(enemy)
	enemy.global_position=get_rand_position()

func get_rand_position():
	var positions=spawnPositions.get_children()
	randomize()
	return positions[randi() % positions.size()].global_position
