extends Area2D

var armor:int=2

const SPEED:int=1
var Enemy_Follow1_Explosion=load(Utility.gEnemy_Follow1_Explosion_path)

func _ready():
	Utility.gEnemy_Follow1 = self
	$AnimationPlayer.current_animation="bomb"

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()

func _process(delta):
	position+=position.direction_to(Utility.gPlayer.position)*SPEED

func _on_Enemy_Follower_body_entered(body):
	fPlayEnteredSound()
	if body != null:
		if armor>0:
			armor-=1
		else:
			Utility.gScore.fUpdateScore(3)
			queue_free()
		body.queue_free()

func _exit_tree():
	if armor >0: return
	var enemy_Follow1_Explosion =Enemy_Follow1_Explosion.instance()
	get_tree().current_scene.add_child(enemy_Follow1_Explosion)
	enemy_Follow1_Explosion.global_position = global_position

func fPlayEnteredSound():
	$AudioAttack.play()

func _on_Enemy_Follower_area_entered(area):
	fPlayEnteredSound()
	if area == Utility.gPlayer:
		Utility.gScore.fUpdateScore(0,-1)
		queue_free()


