extends Area2D

var armor:int=2

const SPEED:int=80
var Enemy1_Explosion=load(Utility.gEnemy1_Explosion_path)

func _ready():
	Utility.gEnemyTrump1 = self

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()

func _process(delta):
	position.x -= SPEED*delta

func _on_EnemyTrump1_area_entered(area):
	fPlayEnteredSound()
	if area == Utility.gPlayer:
		Utility.gScore.fUpdateScore(0,-1)
		queue_free()

func _on_EnemyTrump1_body_entered(body):
	if body != null:
		if armor>0:
			armor-=1
			fPlayEnteredSound()
			Utility.gBidenEffect=false
		else:
			Utility.gScore.fUpdateScore(1)
			Utility.gBidenEffect=true
			queue_free()
		body.queue_free()

func _exit_tree():
	if armor >0: return
	var enemy1_Explosion =Enemy1_Explosion.instance()
	get_tree().current_scene.add_child(enemy1_Explosion)
	enemy1_Explosion.global_position = global_position
	enemy1_Explosion.play()

func fPlayEnteredSound():
	$AudioAttack.play()

