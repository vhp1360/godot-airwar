extends Node2D

var spawnPositions

const Enemy_Follower1=preload("res://Enemy/Enemy_Follow01.tscn")

func _ready():
	Utility.geSpawner_Follow1 = self
	spawnPositions=$Positions

func _on_Timer_timeout():
	if Utility.gPlayerScore>Utility.gFollowerEnemyChangeSpeed: $Timer.wait_time=4
	if Utility.gPlayerScore<=Utility.gShutFollowerEnemy: return
	fSpawn_Enemy_Follow1()

func fSpawn_Enemy_Follow1():
	if Utility.gPlayerScore<Utility.gShutFollowerEnemy: return
	var enemy_follower = Enemy_Follower1.instance()
	get_tree().current_scene.add_child(enemy_follower)
	enemy_follower.global_position=get_rand_position()

func get_rand_position():
	var positions=spawnPositions.get_children()
	randomize()
	return positions[randi() % positions.size()].global_position
