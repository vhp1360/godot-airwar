extends Area2D

const SPEED:int=200

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
func _process(delta):
	position.x -= SPEED*delta


func _on_EnemyTrump3_area_entered(area):
	if area == Utility.gPlayer :
		Utility.gScore.fUpdateScore(-1)
		queue_free()
