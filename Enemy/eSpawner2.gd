extends Node2D

var spawnPositions
const Enemy2=preload("res://Enemy/EnemyTrump2.tscn")

func _ready():
	Utility.geSpawner2 = self
	spawnPositions=$Positions

func _on_Timer_timeout():
	if Utility.gPlayerScore>Utility.gEnemy02ChangeSpeed: $Timer.wait_time=2
	fSpawn_Enemy2()

func fSpawn_Enemy2():
	if Utility.gPlayerScore<=Utility.gShutEnemy02: return
	var enemy = Enemy2.instance()
	get_tree().current_scene.add_child(enemy)
	enemy.global_position=get_rand_position()

func get_rand_position():
	var positions=spawnPositions.get_children()
	randomize()
	return positions[randi()%positions.size()].global_position
