extends Area2D

var armor:int=4
var arrow:int=0
var direction:int=1

const SPEED:int=100
const ALong:int=5

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
func _process(delta):
	position -= Vector2(SPEED*delta,(SPEED*delta)*direction)

func _on_EnemyTrump2_area_entered(area):
	fPlayEnterdSound()
	if area == Utility.gPlayer :
		Utility.gScore.fUpdateScore(0,-1)
		queue_free()

func _on_EnemyTrump2_body_entered(body):
	fPlayEnterdSound()
	if  body != null:
		if armor>0:
			armor-=1
		else:
			Utility.gScore.fUpdateScore(2)
			queue_free()
		body.queue_free()

func _exit_tree():
	pass

func fPlayEnterdSound():
	$AudioAttack.play()

func _on_Timer_timeout():
	direction = -direction
