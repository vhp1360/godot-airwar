extends CanvasLayer

var lbScore:Label
var lbLife: Label
var lbLTop: Label
var lbLPName: Label
var lbLTopest: Label
var Anim:AnimationPlayer
var playImg:TextureRect

func _ready():
	Utility.gPlayerScore=0
	lbScore = $TR_Score/lblScore
	lbLife = $TR_Score/lblLife
	lbLTop = $TR_Score/lblTop
	lbLPName=$TR_Player/lbLPlayerName
	lbLTopest=$TR_Top/lbLTopest
	Anim=$TR_Score/Sprite2/AnimationPlayer
	playImg=$TR_Player/TextureRect
	Utility.gScore = self
	lbLTop.text="TopScore:"+"%03d"%Utility.gPlayerScore
	lbLPName.text=Utility.gPlayerName
	lbLTopest.text= Utility.gInf_dict["TopUser"]+":"+"%03d"%Utility.gInf_dict["TopScore"]
	if Utility.gPlayersName.has(Utility.gPlayerName):
		fSetPlayeImg(Utility.gPlayersScore[Utility.gPlayersName.find(Utility.gPlayerName)])
	else:
		fSetPlayeImg(0)

func fUpdateScore(iScore : int=0,iLife:int=0):
	var newScore: int = iScore + int(lbScore.text)
	var newLife: int = iLife + int(lbLife.text)
	Utility.gPlayerScore+=iScore
	if int(Utility.gPlayerScore % 8)==0: Anim.play("s10")
	elif int(Utility.gPlayerScore % 8)==3: Anim.play("s20")
	if newScore > Utility.gScore2Heart:
		newLife +=1;newScore -=Utility.gScore2Heart
	lbLife.text = "%02d" % newLife
	lbScore.text = "%03d" % newScore
	fSetPlayeImg()
	if newLife < 1:
		queue_free()

func _exit_tree():
	get_tree().change_scene(Utility.gGameOver_path)

func fSetPlayeImg(scoreIn:int=0):
	playImg.texture=load(Utility.fUpdatePlayerImage(scoreIn))
