extends Sprite

func _on_biden_explosion_tree_entered():
	if Utility.gBidenEffect:
		$AnimationPlayer.play("explosion")
		if Utility.gAllowBidenAudio:
			$AudioStreamPlayer2D.play()
			Utility.gAllowBidenAudio=false
	else : queue_free()

func _on_AnimationPlayer_animation_finished(anim_name):
	if $AudioStreamPlayer2D.is_playing(): return
	queue_free()

func _on_AudioStreamPlayer2D_finished():
	if $AnimationPlayer.is_playing(): return
	Utility.gAllowBidenAudio=true
	queue_free()
