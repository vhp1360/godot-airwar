extends RigidBody2D

const Speed = 10

func _ready():
	Utility.gBullet = self

func _process(delta):
	position.x+=Speed
	$bAnim.current_animation="bomb"
	$bAnim.play()

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
