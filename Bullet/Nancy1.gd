extends RigidBody2D

const SPEED=5

func _ready():
	Utility.gNancy1=self

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()

func _process(delta):
	if is_instance_valid(Utility.gEnemy_Follow1):
		if Utility.gEnemy_Follow1 != null and Utility.gEnemy_Follow1.position != null:
			rotation=position.angle_to_point(Utility.gEnemy_Follow1.position)
			position+=position.direction_to(Utility.gEnemy_Follow1.position)*SPEED
	else : position.x+=SPEED

func _on_Nancy1_tree_entered():
	$AnimatedSprite.play("start")
	$AnimatedSprite.play("fire")
	$AnimatedSprite.play("attack")
