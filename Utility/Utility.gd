extends Node2D

var gGamePageLink:String="http://vhp1360.blog.ir/1399/10/15/gamlertrump"
var gWorld1: Node2D
var gCloud: Particles2D
var gpPlayerAnim: AnimatedSprite
var gPlayer: Area2D
var gBullet:RigidBody2D
var gNancy1:RigidBody2D
var gScore: CanvasLayer
var geSpawner1:Node2D
var geSpawner_Follow1:Node2D
var geSpawner2:Node2D
var gEnemyTrump1:Node2D
var gEnemyTrump2:Node2D
var gEnemyTrump3:Node2D
var gEnemy_Follow1:Node2D
var gPlayerList:Node2D
var gGameOver:Control
var winSize:Vector2
var gPlayerPosition:Vector2=Vector2.ZERO
var gNancy1Sound:bool=false
var gBidenEffect:bool=false
var gInf_dict:Dictionary
var gPlayersName:Array
var gPlayersScore:Array
var gPlayerName:String="Anonymouse"
var gPlayerScore:int=0
var gScore2Heart:int=63
var gAllowBidenAudio:bool=true
var gAllowEnemy2Audio:bool=true
var gMainFileName:String="user://gamedata.db"


const gGameOver_path ="res://GameOver/GameOver.tscn"
const gAbout_path ="res://Intro/About.tscn"
const gScore_path= "res://Score/Score.tscn"
const gWorld1_path= "res://World/World1.tscn"
const gEnemy1_Explosion_path="res://Enemy/Enemy1_Explosion.tscn"
const gbiden_explosion_path="res://Bullet/biden_explosion.tscn"
const gEnemy_Follow1_Explosion_path="res://Enemy/Enemy_Follow1_Explosion.tscn"
const gShutEnemy01=0
const gShutEnemy02=11
const gShutFollowerEnemy=39
const gEnemy01ChangeSped=63
const gEnemy02ChangeSpeed=72
const gFollowerEnemyChangeSpeed=111
const gChangePlayerPic=43

onready var utility= self
func _ready():
	winSize=get_viewport_rect().size
	gInf_dict=fReadJson2Dict()
	if gInf_dict==null || gInf_dict.empty():
		gInf_dict={"TopUser":"None","TopScore":0,"PlayersName":[],"PlayersScore":[]}
	gPlayersName=gInf_dict["PlayersName"]
	gPlayersScore=gInf_dict["PlayersScore"]

func fAnim_player(posIn:Vector2,shuttingAnim:String):
	gpPlayerAnim.fAnim_player(posIn,shuttingAnim)

func fCalcTouchMotion(posIn: Vector2)->Vector2 :
	"""
		this function usefull for click out of player
		to move, without dragging
	"""
	var position=get_position_in_parent()
	var SPEED=80
	var RunVec=Vector2.ZERO
	if position.x-posIn.x>=80:
		RunVec -= Vector2(1,0)
	elif -position.x+posIn.x>=80:
		RunVec += Vector2(1,0)
	if position.y-posIn.y>=80:
		RunVec -= Vector2(0,1)
	elif -position.y+posIn.y>=80:
		RunVec += Vector2(0,1)
	if abs(abs(position.x)-abs(posIn.x))<=48:
		RunVec.x=0
	if abs(abs(position.y)-abs(posIn.y))<=48:
		RunVec.y=0
	return SPEED*RunVec

func fSaveLoadObject(content,read_write:int,fileName:String=gMainFileName):
	var file = File.new()
	var content1
	if read_write==1:
		file.open(fileName, File.WRITE)
		file.store_string(content)
	elif read_write==2:
		file.open(fileName, File.READ)
		content1 = file.get_as_text()
	file.close()
	return content1

func fDict2JsonFile(dictIn:Dictionary,fileName:String=gMainFileName)->bool:
	var file = File.new()
	if file.open(fileName,File.WRITE)!=0:
		print("Error opening file")
		return false
	file.store_line(to_json(dictIn))
	file.close()
	return true

func fReadJson2Dict(matchName:String="",seekNo:int=-1,fileName:String=gMainFileName)->Dictionary:
	var file = File.new()
	if !file.file_exists(fileName) || file.open(fileName, File.READ) != 0:
		return {}
	if matchName.empty():
		return parse_json(file.get_line())
	if seekNo!=-1:
		file.seek(seekNo)
		var line=parse_json(file.get_line())
		if fFileLineCheck(line,matchName,file): return line
	else:
		while file.get_position() < file.get_len():
			var line=parse_json(file.get_line())
			if fFileLineCheck(line,matchName,file): return line
	file.close()
	return {}

func fFileLineCheck(lineIn:Dictionary,matchIn:String,fileIn:File)->bool:
	if lineIn != null and typeof(lineIn)==TYPE_DICTIONARY and \
		matchIn==lineIn[lineIn.keys()[0]]:
		fileIn.close()
		return true
	return false

func fFinalWrite():
	fDict2JsonFile(fUpdateGameData())

func fUpdateGameData()->Dictionary:
	if gPlayersName.has(gPlayerName):
		if gPlayerScore>gPlayersScore[gPlayersName.find(gPlayerName)]:
			gPlayersScore[gPlayersName.find(gPlayerName)]=gPlayerScore
	else:
		gPlayersName.append(gPlayerName)
		gPlayersScore.append(gPlayerScore)
	if gInf_dict.has("TopScore") && gPlayerScore>gInf_dict["TopScore"]:
		gInf_dict["TopUser"]=gPlayerName
		gInf_dict["TopScore"]=gPlayerScore
	return {"TopUser":gInf_dict["TopUser"],"TopScore":gInf_dict["TopScore"],
		"PlayersName":gPlayersName,"PlayersScore":gPlayersScore}

func fUpdatePlayerImage(scoreIn:int=0):
	scoreIn=(scoreIn if scoreIn!=0 else gPlayerScore)
	if scoreIn<=Utility.gChangePlayerPic:
		return "res://Assets/png/PlayerImg/p1.png"
	elif scoreIn<=(Utility.gChangePlayerPic*2):
		return "res://Assets/png/PlayerImg/p2.png"
	elif scoreIn<=(Utility.gChangePlayerPic*3):
		return "res://Assets/png/PlayerImg/p3.png"
	elif scoreIn<=(Utility.gChangePlayerPic*4):
		return "res://Assets/png/PlayerImg/p4.png"
	elif scoreIn<=(Utility.gChangePlayerPic*5):
		return "res://Assets/png/PlayerImg/p5.png"
	elif scoreIn<=(Utility.gChangePlayerPic*6):
		return "res://Assets/png/PlayerImg/p6.png"
	elif scoreIn<=(Utility.gChangePlayerPic*7):
		return "res://Assets/png/PlayerImg/p7.png"
	elif scoreIn<=(Utility.gChangePlayerPic*8):
		return "res://Assets/png/PlayerImg/p8.png"
	elif scoreIn<=(Utility.gChangePlayerPic*9):
		return "res://Assets/png/PlayerImg/p9.png"
	else:
		return "res://Assets/png/PlayerImg/p10.png"
