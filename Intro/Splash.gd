extends Node2D

var vey:VideoPlayer
var ghey:VideoPlayer
var Sardar:VideoPlayer
var ann:VideoPlayer
var TR1:TextureRect
var TR2:TextureRect
var TR3:TextureRect
var TR4:TextureRect
var TR5:TextureRect
var whereis:int=0
var fall:bool=false

func _ready():
	vey=$CanvasLayer/vey
	ghey=$CanvasLayer/ghey
	Sardar=$CanvasLayer/Sardar
	ann=$CanvasLayer/ann
	TR1=$CanvasLayer/TextureRect1
	TR2=$CanvasLayer/TextureRect2
	TR3=$CanvasLayer/TextureRect3
	TR4=$CanvasLayer/TextureRect4
	TR5=$CanvasLayer/TextureRect5
	TR1.visible=true

func _process(delta):
	if ann==null:return
	if fall:
		ann.set_position(Vector2(ann.get_position().x,ann.get_position().y+10)*delta)
	if ann.get_position().y>Utility.winSize.y:
		ann.queue_free()
		fall=false

func fSplash():
	if whereis==1:
		TR1.queue_free()
		vey.visible=true
		vey.play()
	elif whereis==2:
		TR2.visible=true
	elif whereis==3:
		TR2.queue_free()
		ghey.visible=true
		ghey.play()
	elif whereis==4:
		TR3.visible=true
	elif whereis==5:
		TR3.queue_free()
		TR4.visible=true
	elif whereis==6:
		TR4.queue_free()
		Sardar.visible=true
		Sardar.play()
	elif whereis==7:
		TR5.visible=true
	elif whereis==8:
		TR5.queue_free()
		ann.visible=true
		ann.play()
	elif whereis==9:
		queue_free()

func _on_vey_finished():
	vey.set_position(Vector2(0,Utility.winSize.y*(5.0/6)))
	vey.set_scale(Vector2(0.2,0.2))
	whereis=2
	fSplash()

func _on_ghey_finished():
	ghey.set_position(Vector2(Utility.winSize.x*(19.0/20),Utility.winSize.y*(5.0/6)))
	ghey.set_scale(Vector2(0.2,0.2))
	whereis=4
	fSplash()

func _on_Timer_timeout():
	fSplash()

func _on_TextureRect2_visibility_changed():
	$Timer.wait_time=5
	whereis=3
	$Timer.start()

func _on_TextureRect3_visibility_changed():
	$Timer.wait_time=12
	whereis=5
	$Timer.start()
func _on_TextureRect4_visibility_changed():
	$Timer.wait_time=12
	whereis=6
	$Timer.start()


func _on_Sardar_finished():
	Sardar.set_position(Vector2(0,0))
	Sardar.set_size(Vector2(Utility.winSize.x,Utility.winSize.y/2.0))
	whereis=7
	fSplash()

func _on_TextureRect5_visibility_changed():
	$Timer.wait_time=4
	whereis=8
	$Timer.start()


func _on_ann_finished():
	fall=true
	whereis=9
	fSplash()

func _exit_tree():
	get_tree().change_scene("res://Intro/PlayersList.tscn")
	queue_free()

#func _on_ann_tree_exited():
#	pass # Replace with function body.

func _on_TextureRect1_visibility_changed():
	$Timer.wait_time=4
	whereis=1
	$Timer.start()
