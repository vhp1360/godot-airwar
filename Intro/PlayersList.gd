extends Node2D

var itemList:ItemList
var lblMessage:RichTextLabel
var lineEdit:LineEdit

func _ready():
	Utility.gPlayerList=self
	itemList=$CanvasLayer/VBoxContainer/ItemList
	lblMessage=$CanvasLayer/VBoxContainer/LblMessage
	lineEdit=$CanvasLayer/VBoxContainer/HBoxContainer/LineEdit
	lineEdit.grab_focus()
	for q in range(0,Utility.gPlayersName.size()):
		itemList.add_item("%05s"%Utility.gPlayersScore[q]+"%35s" %Utility.gPlayersName[q])

func _on_TextureRect_pressed():
	fTextInput()

func _on_LblMessage_focus_exited():
	lblMessage.visible=false

func _on_ItemList_item_activated(index):
	Utility.gPlayerName=Utility.gPlayersName[index]
	Utility.gPlayerScore=Utility.gPlayersScore[index]
	self.queue_free()
	get_tree().change_scene(Utility.gWorld1_path)

func fTextInput():
	var pName:String=$CanvasLayer/VBoxContainer/HBoxContainer/LineEdit.text
	if Utility.gPlayersName.has(pName):
		lblMessage.append_bbcode("[wave amp=50 freq=2]the [/wave]"+ \
			"[shake rate=5 level=10][b][color=red]"+pName+"[/color][b][/shake]"+ \
			"[wave amp=50 freq=2] Already Exist in List[/wave]")
		lblMessage.visible=true
		lblMessage.grab_focus()
	else:
		Utility.gPlayerName=pName
		Utility.gPlayerScore=0
		self.queue_free()
		get_tree().change_scene(Utility.gWorld1_path)



func _on_LineEdit_gui_input(event):
	if event is InputEventKey and event.pressed:
		if event.scancode == KEY_ENTER:
			fTextInput()
