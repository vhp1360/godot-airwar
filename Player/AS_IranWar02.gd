extends AnimatedSprite

var audioPlayer={Vector2(1,0):[load("res://Assets/Sounds/forward.ogg"),"Go"],
				Vector2(0,1):[load("res://Assets/Sounds/forward.ogg"),"Up"],
				Vector2(-1,0):[load("res://Assets/Sounds/back.ogg"),"Back"],
				Vector2(0,-1):[load("res://Assets/Sounds/back.ogg"),"Down"],
				Vector2(1,1):[load("res://Assets/Sounds/forward.ogg"),"FU"],
				Vector2(1,-1):[load("res://Assets/Sounds/forward.ogg"),"FD"],
				Vector2(-1,-1):[load("res://Assets/Sounds/back.ogg"),"DB"],
				Vector2(-1,1):[load("res://Assets/Sounds/back.ogg"),"DU"]
				}

var audioShut={"Shut":load("res://Assets/Sounds/shut.wav"),
				"Shut1":load("res://Assets/Sounds/ShutAtom1.wav"),
				"Shut2":null,"Shut3":null}

func _ready():
	Utility.gpPlayerAnim = self

func fAnim_player(posIn:Vector2,shuttingAnim:String=""):
	if audioPlayer.has(posIn):
		$PlayerMoving.stream=audioPlayer.get(posIn)[0]
		$PlayerMoving.play()
		play(audioPlayer.get(posIn)[1])
	else:
		$PlayerMoving.stop()
		play("Idle")

	if audioShut.has(shuttingAnim):
		$PlayerShutting.stream=audioShut.get(shuttingAnim)
		$PlayerShutting.play()
