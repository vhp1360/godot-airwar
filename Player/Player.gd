extends Area2D

var playerAnim:String="Idle"
var shutAnim:String="Shut"
var motion:Vector2=Vector2.ZERO
var isTimeout:bool=true
var touchPostion:Vector2=Vector2.ZERO
var winSize:Vector2
var areaLimit:float=48.0
var playerCollid:CollisionPolygon2D
const SPEED = 8
const Bullet=preload("res://Bullet/Bullet.tscn")
const Nancy1=preload("res://Bullet/Nancy1.tscn")
const TouchDistance=18

func _ready():
	Utility.gPlayer = self
	winSize=Utility.winSize
	playerCollid=CollisionPolygon2D.new()
	playerCollid.set_polygon(Utility.gpPlayerAnim.get_node("CollisionPolygon2D").polygon)
	add_child(playerCollid)

func _input(event):
	if event is InputEventScreenTouch:
		if event.is_pressed() and event.position.x > winSize.x/2:
			fTouchShutEvent(true)
		elif ! event.is_pressed() and event.position.x > winSize.x/2:
				fTouchShutEvent(false)
	elif event is InputEventScreenDrag:
		fMotion(event.position-position,1)

func fTouchShutEvent(isPressing:bool):
	if isPressing:
		$Timer.start()
		isTimeout=false
	elif ! isTimeout:
		fFire()
		fFollowFire()

func _process(delta):
	fShut()
	fVelocity()
	fMotion(motion,delta)

func fVelocity():
	if Input.is_action_pressed("ui_right"): motion.x+=SPEED
	elif Input.is_action_pressed("ui_left"): motion.x-=SPEED
	else : motion.x = 0
	if Input.is_action_pressed("ui_up"): motion.y-=SPEED
	elif Input.is_action_pressed("ui_down"): motion.y+=SPEED
	else : motion.y = 0

func fMotion(newPos:Vector2,delta:float=1.0):
	position=fCheckAreaLimit(position+newPos*delta)
	Utility.fAnim_player(newPos.normalized(),"")

func fShut():
	if Input.is_action_just_released("ui_fire"): fFire()
	if Input.is_action_just_released("ui_follow_fire"): fFollowFire()

func fFire():
	var bullet = Bullet.instance()
	get_tree().current_scene.add_child(bullet)
	bullet.global_position=Vector2(global_position.x+18,global_position.y+10)

func fFollowFire():
	if Utility.gPlayerScore < Utility.gShutFollowerEnemy: return
	if $Timer4FollowEnemy.is_stopped():
		var followbullet = Nancy1.instance()
		get_tree().current_scene.add_child(followbullet)
		followbullet.global_position=Vector2(global_position.x,global_position.y+18)
		$Timer4FollowEnemy.start()

func _on_Timer_timeout():
	isTimeout=true

func fCheckAreaLimit(posIn:Vector2)->Vector2:
	if posIn.x<areaLimit: posIn.x=areaLimit
	elif posIn.x>(winSize.x-areaLimit): posIn.x=winSize.x-areaLimit
	if posIn.y<areaLimit: posIn.y=areaLimit
	elif posIn.y>(winSize.y-areaLimit): posIn.y=winSize.y-areaLimit
	return posIn

#func _on_Timer4FollowEnemy_timeout():
#	pass # Replace with function body.
